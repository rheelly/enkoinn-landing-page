This is the repository for the enkoinn landing page. 

Structural notes: 

- All content goes into the `index.html` and this should stay as the only html file in this project
- CSS files are separated by which section they cover and all responsive css for each section can be found in the corresponding css file not in an overall `responive.css` file or something like that
- JS files are separated the same way css files are but by functionality and not only by section