/**
 * This variable stores the text for the small description when clicking 
 * on an icon. The text for the modals is stored in "modalDescription.js"
 */
const smallTexts = {
    financialText: "We know how hard it can be to just start out in the streaming world. We hope we can make it a little bit easier! " +
    "<br> <a class='modal-link' id='financial-link' onclick='showModal(this)'> Find out more. </a>",
    technicalText: "When you're just overwhelmed with the myriad options on Streamlabs or don't know how to use this and that bot, we're here to help. " +
    "<br> <a class='modal-link' id='technical-link' onclick='showModal(this)'> Find out more. </a>",
    gearText: "While gear is often not the most important point when starting out, a good mic or some good light can definitely improve a stream. " + 
    "<br> <a class='modal-link' id='gear-link' onclick='showModal(this)'> Find out more. </a>",
    visualText: "Some good looking and more importantly recognizable art can make people more likely to join or find your stream. " +
    "<br> <a class='modal-link' id='visual-link' onclick='showModal(this)'> Find out more. </a>",
    conceptualText: "Likely the most important point we help you analyze what concept is behind your stream, try to give you tips and what to improve and what is already good! " + 
    "<br> <a class='modal-link' id='conceptual-link' onclick='showModal(this)'> Find out more. </a>",
}
const smallHeadlines = {
    financialHeadline: "Financial support",
    technicalHeadline: "Technical support",
    gearHeadline: "Gear support",
    visualHeadline: "Visual support",
    conceptualHeadline: "Conceptual support"
}