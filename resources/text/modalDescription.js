/**
 * This variable stores the text for the modal (headline and content) when a 
 * "find out more" link of a pillar is clicked
 */
const modalDescription = {
    financialText: "Starting out as a streamer can be hard. Nowadays a lot of time is expected " + 
    "to be put into stream, even if you're just starting out. For that reason we'd like to  " + 
    "support you on your endeavor. This support can reach from monthly support to just paying your" + 
    "Discord Nitro subscription. <br> " + 
    "If you think you have a nice concept or you're actually good at streaming, then get into" + 
    "contact with us and we can discuss the details. <br> " + 
    "Do note however that this is not simply a loan or we're handing out money. " + 
    "There are a lot of people out there and we cant support all of them, so please be aware  " + 
    "that there can be a multitude of reasons why we cant support you. As, of course, this offer " +
    "is extremly limited. ",
    technicalText: "There is a lot of stuff which can be optimized in a Stream.  " + 
    "From the correct bitrate, to what codec to use, to how and what microphone you use, " + 
    "- or one of the most important steps - how you handle your audio levels. " + 
    "We can help, explain or make you better understand alot of these topics and " + 
    "won't stop at just explaining. If you want to get your audio levels perfect, its best " + 
    "to have people actually watching your stream while you tinker with that. " + 
    "We can also do that and more! ",
    gearText: "There is so much hardware out there. While back in the day you just had " + 
    "to worry about your pc, mic (and lighting), nowadays there are also streamdecks, green screen setups " + 
    "or a boatload of cameras to choose from. Well if you're feeling overwhelmed we're either here to tell " + 
    "you not to worry about all this stuff or if you're really eager to get it, get it for you. " + 
    "If you're just starting out and need a basic setup or if you already have some people watching you and decide" + 
    "its time for a better camera. We can get that for you, so you don't have to worry about what to choose " + 
    "or about the money for getting it.  " + 
    "Of course, as with every point, we can't just get stuff for everyone, so please be aware " + 
    "that if you're not serious, we also can't be! ",
    visualText: "While this is definitely not the most important point when starting out " + 
    "it can also not be underestimated how much effect a recognizable logo/icon/etc has " + 
    "for not only finding your stream, but often more important finding your stream again" + 
    "after having lurked one time. <br>" + 
    "We can help you with all kinds of visual designs, from your logo, to a twitch banner or" + 
    "even your whole Design Identity. We're here to help! ",
    conceptualText: "This point is about us watching your stream, listening to your ideas or concepts " + 
    "and giving you feedback for all of it. This is likely the most important point when starting out. " + 
    "Having some people watch your stream, point out what could be better and point out what strengths" + 
    "to capitalize. <br> " + 
    "Then you're prepared for the real thing and random lurkers won't just drop your stream, because of " + 
    "that one annoying habit of yours or because you're stream is just plain too boring. ",
    impressumText: "Legal Disclosure | Information in accordance with Section 5 TMG <br>" + 
    "Daniel Stehlik <br> Landwehrstra&szlig;e 43<br> 64293 Darmstadt <br> <br> " + 
    "Contact Information<br> Telephone: +4917680810284<br> Fax: +4917680810284<br> E-Mail: <a href='mailto:info@rheelly.net'>info@rheelly.net</a><br> <br>" + 
    "Disclaimer <br> " + 
    "Accountability for content<br> " +
    "The contents of our pages have been created with the utmost care. However, we cannot guarantee the contents " + 
    "accuracy, completeness or topicality. According to statutory provisions, we are furthermore responsible for " +
    "our own content on these web pages. In this matter, please note that we are not obliged to monitor " +
    "the transmitted or saved information of third parties, or investigate circumstances pointing to illegal activity. " +
    "Our obligations to remove or block the use of information under generally applicable laws remain unaffected by this as per " +
    "§§ 8 to 10 of the Telemedia Act (TMG). <br> <br>" + 
    "Accountability for links <br>" + 
    "Our web pages and their contents are subject to German copyright law. <br> " + 
    "No violations were evident to us at the time of linking. Should any legal infringement become known to us, we will remove the respective link immediately. <br> <br>" + 
    "Copyright<br> " +
    "Unless expressly permitted by law, every form of utilizing, reproducing or processing " +
    "works subject to copyright protection on our web pages requires the prior consent of the respective owner of the rights. " +
    "Individual reproductions of a work are only allowed for private use. " +
    "The materials from these pages are copyrighted and any unauthorized use may violate copyright laws.",
}

const modalHeadlines = {
    financialHeadline: "Financial support",
    technicalHeadline: "Technical support",
    gearHeadline: "Gear support",
    visualHeadline: "Visual support",
    conceptualHeadline: "Conceptual support",
    impressumHeadline: "Impressum"
}