/********************************************/
/* Handles the modals that acts as the      */
/* contact form.                            */
/********************************************/

function showContactFormular() {
    const modal = document.getElementById("contact-formular");
  
    modal.velocity({ display: 'block' }, { duration: 0 });
    modal.velocity({ opacity: 1 }, { duration: 300 })
  }