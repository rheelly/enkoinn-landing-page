/********************************************/
/* Handles animating the text of the five   */
/* pillars. NOT the actual animation, this  */
/* can be found in fivePillarAnimation.js   */
/********************************************/

// This variable stores the icon that is currently shown / in the middle of the carousel
// Basically the icon that is currently selected
let currentlySelected;

/**
 * Shows headline when hovering over a pillar icon
 * @param {*} supportType 
 */
function showHeadline(supportType) {
    const substring = supportType.id.split("-");
    const actualType = substring[0];

    const headlineType = actualType + "-headline";
    const headline = document.getElementById(headlineType);
    headline.velocity({ opacity: 1 }, { duration: 200 });
}

/**
 * Hides headline when not hovering over a pillar icon
 * @param {*} supportType 
 */
function hideHeadline(supportType) {
    // If a pillar is currently selected its
    // headline should not change back to 0 opacity
    if (supportType.id === currentlySelected) {
        return;
    }

    const substring = supportType.id.split("-");
    const actualType = substring[0];

    const headlineType = actualType + "-headline";
    const headline = document.getElementById(headlineType);
    headline.velocity({ opacity: 0 }, { duration: 200 });
}

/**
 * Dependent on the clicked icon, shows text and link to find out more for this pillar
 * 
 * @param {*} supportType 
 */
function showMoreText(supportType) {
    const substring = supportType.id.split("-");
    const actualType = substring[0];

    // Get text element
    const textType = actualType + "-text";
    const text = document.getElementById(textType);

    // Get headline element
    const headlineType = actualType + "-headline";
    const headline = document.getElementById(headlineType);

    // Load correct text, dependent on which icon is clicked 
    switch (supportType.id) {
        case "financial-icon":
            text.innerHTML = smallTexts.financialText;
            break;
        case "technical-icon":
            text.innerHTML = smallTexts.technicalText;
            break;
        case "gear-icon":
            text.innerHTML = smallTexts.gearText;
            break;
        case "conceptual-icon":
            text.innerHTML = smallTexts.conceptualText;
            break;
        case "visual-icon":
            text.innerHTML = smallTexts.visualText;
            break;
        default:
        // do nothing
    }
    // Change all headlines opacity to 0
    // (so all other headlines are not visible all the time)
    $('.headline').velocity({ opacity: 0, visibility: "hidden" }, { duration: 0 });
    $('.headline').parent().css("visibility", "hidden");

    $('.description').velocity({ opacity: 0, visibility: "hidden" }, { duration: 0 });
    $('.description').parent().css("visibility", "hidden");

    text.parentNode.velocity({ visibility: "visible" });
    text.velocity({ opacity: 1, visibility: "visible" });
    headline.velocity({ opacity: 1, visibility: "visible" });

    // This is necessary so the headlines opacity does not change back 
    // to 0 when a "mouseout" event is triggered
    currentlySelected = supportType.id;
}