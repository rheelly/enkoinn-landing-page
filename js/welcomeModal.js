/********************************************/
/* Handles the animation to the actual      */
/* main part of the website and the links   */
/* on the welcome modal (discord, rheel.ly) */
/********************************************/

// when clicking on the modal itself
$('#welcomeModal').click(function () {
    // Five pillars modal
    const modal = document.getElementById("welcomeModal");
    modal.velocity({ opacity: 0 }, { duration: 300 });
    modal.velocity({ display: 'none' }, { duration: 0 });
});
// When clicking on the 'What we're offering' button
$('#toMainContentButton').click(function () {
    // Five pillars modal
    const modal = document.getElementById("welcomeModal");
    modal.velocity({ opacity: 0 }, { duration: 300 });
    modal.velocity({ display: 'none' }, { duration: 0 });
});

$('#discordButton').click(function () {
    window.open(
        'https://discord.gg/Ze7nSSu',
        '_blank'
    );
});