/********************************************/
/* Handles the modals that appear when      */
/* clicking on a specific five pillar       */
/* icon.                                    */
/********************************************/

function showModal(supportType) {
	// Get modal element to show it
	const modal = document.getElementById('fivePillarsModal');

	// Get headline element to fill it with correct headline text
	const headline = document.getElementById('modal-headline');
	// Get content text element to fill it with the correct content text
	const text = document.getElementById('modal-content-text');

	modal.velocity({ display: 'block' }, { duration: 0 });
	modal.velocity({ opacity: 1 }, { duration: 300 });

	// The switch decides which text is displayed in the modal
	switch (supportType.id) {
		case 'financial-link':
			headline.innerHTML = modalHeadlines.financialHeadline;
			text.innerHTML = modalDescription.financialText;
			break;
		case 'technical-link':
			headline.innerHTML = modalHeadlines.technicalHeadline;
			text.innerHTML = modalDescription.technicalText;
			break;
		case 'gear-link':
			headline.innerHTML = modalHeadlines.gearHeadline;
			text.innerHTML = modalDescription.gearText;
			break;
		case 'conceptual-link':
			headline.innerHTML = modalHeadlines.conceptualHeadline;
			text.innerHTML = modalDescription.conceptualText;
			break;
		case 'visual-link':
			headline.innerHTML = modalHeadlines.visualHeadline;
			text.innerHTML = modalDescription.visualText;
			break;
		case 'impressum-link':
			headline.innerHTML = modalHeadlines.impressumHeadline;
			text.innerHTML = modalDescription.impressumText;
		default:
		// do nothing
	}
}

function closeModalOnBody() {
	const modal = document.getElementById('fivePillarsModal');
	if(modal.style.opacity >= 1) {
		modal.velocity({ opacity: 0 }, { duration: 300 });
		modal.velocity({ display: 'none' }, { duration: 0 });
	}
}
