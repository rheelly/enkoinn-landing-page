/********************************************/
/* Every function that needs to be called   */
/* when the site has loaded should be called*/
/* in here.                                 */
/********************************************/

$(document).ready(function () {
	// Calling the rain functions for the welcome modal
	$('body').toggleClass('splat-toggle');
	$('.splat-toggle.toggle').toggleClass('active');
	makeItRain();

	// Calling the functions to trigger the modal for each
	// of the information icons
	$('.modal-hide-button').click(function () {
		// Five pillars modal
		const modal = document.getElementById('fivePillarsModal');
		modal.velocity({ opacity: 0 }, { duration: 300 });
		modal.velocity({ display: 'none' }, { duration: 0 });

		// Contact formular
		const contactFormular = document.getElementById('contact-formular');
		contactFormular.velocity({ opacity: 0 }, { duration: 300 });
		contactFormular.velocity({ display: 'none' }, { duration: 0 });
	});
	$(document).click(function (e) {
		if($(e.target).is('#mainContainer')) {
			console.log('test');
		}
		if($(e.target).is('.modal')) {
			console.log('is modal');
		}
	});
});


function showTwitchChannel() {
	window.open(
        'https://twitch.tv/saltyinn',
        '_blank'
    );
}

function discordLink() {
	window.open(
        'https://discord.gg/346Ftcp',
        '_blank'
    );
}

function reloadPage() {
	location.reload();
}